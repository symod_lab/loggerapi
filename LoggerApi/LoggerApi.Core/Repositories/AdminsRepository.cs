﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LoggerApi.Core.Models;
using LoggerApi.Core.Models.Db;
using Microsoft.EntityFrameworkCore;

namespace LoggerApi.Core.Repositories
{
    public class AdminsRepository
    {
        private readonly LoggerContext _context;

        public AdminsRepository(LoggerContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Admin>> GetAll()
        {
            return await _context.Admins.ToArrayAsync();
        }

        public async Task<Admin> Create(Admin admin)
        {
            var created = await _context.Admins.AddAsync(admin);
            await _context.SaveChangesAsync();
            return created.Entity;
        }

        public async Task<Admin> GetAdminById(Guid id)
        {
            return await _context.Admins.FirstAsync(x => x.Id == id);
        }

        public async Task<bool> AdminExists(Guid id)
        {
            return await _context.Admins.AnyAsync(x => x.Id == id);
        }

        public async Task Delete(Guid id)
        {
            var admin = await _context.Admins.FirstAsync(x=>x.Id == id);
            _context.Admins.Remove(admin);
            await _context.SaveChangesAsync();
        }
    }
}
