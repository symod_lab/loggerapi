﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggerApi.Core.Models;
using LoggerApi.Core.Models.Db;
using Microsoft.EntityFrameworkCore;

namespace LoggerApi.Core.Repositories
{
    public class ClientsRepository
    {
        private readonly LoggerContext _context;

        public ClientsRepository(LoggerContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<ClientInfo>> GetAllClientsInfo() 
            => await _context.Clients.ToListAsync();

        public async Task<IEnumerable<ClientInfo>> GetClientsInfoById(Guid id) 
            => await _context.Clients
                .Include(x => x.Identity)
                .Where(x=>x.Admin.Id == id)
                .ToListAsync();

        public Task<ClientInfo> GetClientInfoByIdentityId(Guid id) 
            => _context.Clients.Include(x=>x.Identity).FirstAsync(x=>x.Identity.Id == id);

        public Task<bool> UserInfoExists(Guid id) 
            => _context.Clients.AnyAsync(x => x.Id == id);

        public Task<ClientIdentity> GetIdentity(ClientIdentity clientIdentity) 
            => _context.ClientsIdentities
                .FirstAsync(x => x.Login == clientIdentity.Login && x.Pass == clientIdentity.Pass);

        public Task<bool> UserIdentityExists(ClientIdentity clientIdentity) 
            => _context.ClientsIdentities
                .AnyAsync(x => x.Login == clientIdentity.Login && x.Pass == clientIdentity.Pass);

        public async Task<ClientInfo> Register(ClientInfo newClient)
        {
            var registered = await _context.Clients.AddAsync(newClient);
            await _context.SaveChangesAsync();
            return registered.Entity;
        }

        public async Task SetUserInfo(ClientInfo info)
        {
            var clientinfo = await _context.Clients.FirstAsync(x => x.IdentityId == info.IdentityId);
            //var back = clientinfo.ClientInfo;
            clientinfo.Name = info.Name;
            clientinfo.Ip = info.Ip;
            //_context.Clients.Remove(back);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var client = await _context.Clients.FirstAsync(x=>x.Id == id);
            _context.Clients.Remove(client);
            await _context.SaveChangesAsync();
        }
    }
}
