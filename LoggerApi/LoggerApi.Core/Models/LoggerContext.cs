﻿using LoggerApi.Core.Models.Db;
using Microsoft.EntityFrameworkCore;

namespace LoggerApi.Core.Models
{
    public class LoggerContext : DbContext
    {
        public LoggerContext(DbContextOptions<LoggerContext> options) : base(options){}

        public DbSet<Admin> Admins { get; set; }

        public DbSet<ClientInfo> Clients { get; set; }
        public DbSet<ClientIdentity> ClientsIdentities { get; set; }

        public DbSet<Config> Configs { get; set; }

    }
}
