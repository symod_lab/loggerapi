﻿using System.ComponentModel.DataAnnotations;

namespace LoggerApi.Core.Models.Db
{
    public class Config
    {
        [Key]
        public string Name { get; set; }
        public string Parameter { get; set; }
    }
}
