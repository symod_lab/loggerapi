﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LoggerApi.Core.Models.Db
{
    public class ClientInfo
    {
        [Key]
        public Guid Id { get; set; }
        public string Ip { get; set; }
        public string Name { get; set; }

        public Guid IdentityId { get; set; }
        public ClientIdentity Identity { get; set; }

        public Admin Admin { get; set; }
    }
}
