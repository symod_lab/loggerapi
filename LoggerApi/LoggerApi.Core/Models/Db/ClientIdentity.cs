﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LoggerApi.Core.Models.Db
{
    public class ClientIdentity
    {
        [Key]
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string EMail { get; set; }
        public string Pass { get; set; }
        public ClientInfo ClientInfo { get; set; }
    }
}
