﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LoggerApi.Core.Models.Db
{
    public class Admin
    {
        [Key]
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string EMail { get; set; }
        public string Pass { get; set; }
        public virtual ICollection<ClientInfo> Clients { get; set; }
    }
}
