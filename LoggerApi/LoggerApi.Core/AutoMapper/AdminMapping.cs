﻿using AutoMapper;
using LoggerApi.Core.Models.Db;
using LoggerApi.Dto.Admin.Requests;
using LoggerApi.Dto.Admin.Responses;

namespace LoggerApi.Core.AutoMapper
{
    public class AdminMapping : Profile
    {
        public AdminMapping()
        {
            CreateRequestsMaps();
            CreateResponseMaps();
        }

        private void CreateRequestsMaps()
        {
            CreateMap<AdminRegistrationRequest, Admin>();
        }

        private void CreateResponseMaps()
        {
            CreateMap<Admin, AdminMinInfoResponse>();
        }
    }
}
