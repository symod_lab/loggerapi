﻿using AutoMapper;
using LoggerApi.Core.Models.Db;
using LoggerApi.Dto.Clients.Requests;
using LoggerApi.Dto.Clients.Responses;

namespace LoggerApi.Core.AutoMapper
{
    public class ClientMapping : Profile
    {
        public ClientMapping()
        {
            RequestsMapping();
            ResponsesMapping();
        }

        private void ResponsesMapping()
        {
            //Client info
            CreateMap<ClientInfo, SecureClientInfoResponse>()
                .ForMember(x=>x.Id, y=>y.MapFrom(info => info.IdentityId));

            CreateMap<ClientInfo, FullClientInfoResponse>()
                .ForMember(x => x.Login, y => y.MapFrom(target => target.Identity.Login))
                .ForMember(x => x.EMail, y => y.MapFrom(target => target.Identity.EMail));

            //ClientIdentity
        }

        private void RequestsMapping()
        {
            //Client info
            CreateMap<NewUserInfoRequest, ClientInfo>()
                .ForMember(x=>x.IdentityId, y=>y.MapFrom(info => info.Id));

            //ClientIdentity
            CreateMap<ClientRegistrationRequest, ClientIdentity>();
            CreateMap<ClientLoginRequest, ClientIdentity>();
        }
    }
}
