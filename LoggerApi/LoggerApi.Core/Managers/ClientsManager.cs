﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using LoggerApi.Core.Models.Db;
using LoggerApi.Core.Repositories;
using LoggerApi.Dto.Clients.Requests;
using LoggerApi.Dto.Clients.Responses;

namespace LoggerApi.Core.Managers
{
    public class ClientsManager
    {
        private readonly ClientsRepository _clientsRepository;
        private readonly AdminsRepository _adminsRepository;
        private readonly IMapper _mapper;

        public ClientsManager(ClientsRepository clientsRepository, AdminsRepository adminsRepository, IMapper mapper)
        {
            _clientsRepository = clientsRepository;
            _mapper = mapper;
            _adminsRepository = adminsRepository;
        }

        public async Task<IEnumerable<SecureClientInfoResponse>> GetAllClientsInfo()
        {
            var clientsInfo = await _clientsRepository.GetAllClientsInfo();
            var secureInfo = _mapper.Map<IEnumerable<SecureClientInfoResponse>>(clientsInfo);
            return secureInfo;
        }

        public async Task<IEnumerable<FullClientInfoResponse>> GetAllClientsInfo(Guid adminId)
        {
            var clientsInfo = await _clientsRepository.GetClientsInfoById(adminId);

            var secureInfo = _mapper.Map<IEnumerable<FullClientInfoResponse>>(clientsInfo);
            return secureInfo;
        }
        //public async Task<ClientIdentity> GetIdentity(Object identityRequest)
        //{
        //    var obj = _mapper.Map<ClientIdentity>(identityRequest);
        //    if(await _clientsRepository.UserIdentityExists(obj)) throw new Exception();

        //    return await _clientsRepository.GetIdentity(obj);
        //}

        public async Task<IEnumerable<FullClientInfoResponse>> GetClientsInfoById(Guid id)
        {
            var info = await _clientsRepository.GetClientInfoByIdentityId(id);
            var response = _mapper.Map<IEnumerable<FullClientInfoResponse>>(info);
            return response;
        }

        public async Task<FullClientInfoResponse> GetClientInfoByIdentityId(Guid id)
        {
            var info = await _clientsRepository.GetClientInfoByIdentityId(id);

            var response = _mapper.Map<FullClientInfoResponse>(info);
            return response;
        }

        public async Task<FullClientInfoResponse> Register(ClientRegistrationRequest registrationUser)
        {
            var newClient = _mapper.Map<ClientIdentity>(registrationUser);

            var admin = await _adminsRepository.GetAdminById(registrationUser.AdminId);
            var registered = await _clientsRepository.Register(new ClientInfo
            {
                Admin = admin,
                Identity = newClient
            });
            var response = _mapper.Map<FullClientInfoResponse>(registered);
            return response;
        }

        public Task SetUserInfo(NewUserInfoRequest newUserInfo)
        {
            var info = _mapper.Map<ClientInfo>(newUserInfo);
            return _clientsRepository.SetUserInfo(info);
        }

        public Task<bool> Login(ClientLoginRequest credentials)
        {
            var info = _mapper.Map<ClientIdentity>(credentials);
            return _clientsRepository.UserIdentityExists(info);
        }

        public Task SignOut(Guid id) 
            => _clientsRepository.Delete(id);
    }
}
