﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using LoggerApi.Core.Models.Db;
using LoggerApi.Core.Repositories;
using LoggerApi.Dto.Admin.Requests;
using LoggerApi.Dto.Admin.Responses;

namespace LoggerApi.Core.Managers
{
    public class AdminsManager
    {
        private readonly AdminsRepository _adminsRepository;
        private readonly IMapper _mapper;

        public AdminsManager(AdminsRepository adminsRepository, IMapper mapper)
        {
            _adminsRepository = adminsRepository;
            _mapper = mapper;
        }

        public async Task<AdminInfoResponse> Register(AdminRegistrationRequest newAdmin)
        {
            var admin = _mapper.Map<Admin>(newAdmin);
            var adminInfo = await _adminsRepository.Create(admin);
            var response = _mapper.Map<AdminInfoResponse>(adminInfo);
            return response; 
        }

        public async Task<IEnumerable<AdminMinInfoResponse>> GetAll()
        {
            var data = await _adminsRepository.GetAll();
            var response = _mapper.Map<IEnumerable<AdminMinInfoResponse>>(data);
            return response;
        }

        public async Task<AdminInfoResponse> FindAdmin(Guid id)
        {
            var fullAdmin = await _adminsRepository.GetAdminById(id);
            var saveAdmin = _mapper.Map<AdminInfoResponse>(fullAdmin);
            return saveAdmin;
        }

        public async Task Delete(Guid id)
        {
            await _adminsRepository.Delete(id);
        }
    }
}
