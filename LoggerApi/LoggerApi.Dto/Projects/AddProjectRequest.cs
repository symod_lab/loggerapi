﻿using System;

namespace LoggerApi.Dto.Projects
{
    public class AddProjectRequest
    {
        public Guid AdminId { get; set; } 
        public string ProjectName { get; set; }
    }
}
