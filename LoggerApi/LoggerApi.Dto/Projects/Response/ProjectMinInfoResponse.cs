﻿using System;

namespace LoggerApi.Dto.Projects.Response
{
    public class ProjectMinInfoResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public string Url { get; set; }
    }
}
