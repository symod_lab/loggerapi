﻿using System;

namespace LoggerApi.Dto.Admin.Responses
{
    public class AdminInfoResponse
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string EMail { get; set; }
        //public IEnumerable<ProjectMinInfoResponse> Projects { get; set; }
    }
}
