﻿using System;

namespace LoggerApi.Dto.Admin.Responses
{
    public class AdminMinInfoResponse
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string EMail { get; set; }
    }
}
