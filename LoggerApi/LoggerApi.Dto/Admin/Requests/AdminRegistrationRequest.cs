﻿namespace LoggerApi.Dto.Admin.Requests
{
    public class AdminRegistrationRequest
    {
        public string Login { get; set; }
        public string EMail { get; set; }
        public string Pass { get; set; }
    }
}
