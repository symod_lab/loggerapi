﻿using System;

namespace LoggerApi.Dto.Clients.Responses
{
    public class SecureClientInfoResponse
    {
        public Guid Id { get; set; }
        public string Ip { get; set; }
        public string Name { get; set; }
    }
}
