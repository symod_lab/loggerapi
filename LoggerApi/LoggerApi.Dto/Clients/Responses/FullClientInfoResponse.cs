﻿using System;

namespace LoggerApi.Dto.Clients.Responses
{
    public class FullClientInfoResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string EMail { get; set; }
        public string Ip { get; set; }
    }
}
