﻿using System;

namespace LoggerApi.Dto.Clients.Requests
{
    public class NewUserInfoRequest
    {
        public Guid Id { get; set; }
        public string Ip { get; set; }
        public string Name { get; set; }
    }
}
