﻿using System;

namespace LoggerApi.Dto.Clients.Requests
{
    public class ClientRegistrationRequest
    {
        public string Login { get; set; }
        public string Email { get; set; }
        public string Pass { get; set; }
        public Guid AdminId { get; set; }
    }
}
