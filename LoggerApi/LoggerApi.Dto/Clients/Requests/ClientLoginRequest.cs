﻿namespace LoggerApi.Dto.Clients.Requests
{
    public class ClientLoginRequest
    {
        public string Login { get; set; }
        public string Pass { get; set; }
    }
}
