﻿using System.Reflection;
using AutoMapper;
using FluentValidation.AspNetCore;
using LoggerApi.Core.Models;
using LoggerApi.DIConfigs;
using LoggerApi.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace LoggerApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper();

            services.AddMvc()
                .AddFluentValidation(fvc => fvc.RegisterValidatorsFromAssemblyContaining<Startup>())
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Logger api", Version = "v1" });
            });

            var type = Configuration.GetConnectionString("Type");
            if (type.ToLower() == "mssql")
            {
                services.AddDbContext<LoggerContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("MsSqlLocalLoggerDbConnection"), b => b.MigrationsAssembly(Assembly.GetAssembly(typeof(LoggerContext)).FullName)));
                
            }
            else
            {
                services.AddDbContext<LoggerContext>(options =>
                    options.UseMySQL(Configuration.GetConnectionString("MySqlLocalLoggerDbConnection")));
            }


            RepositoryRegistrations.Register(services);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMiddleware<ExceptionHandlerMiddleware>();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Logger api");
            });
            
            // app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
