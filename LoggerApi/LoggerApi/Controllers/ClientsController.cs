﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LoggerApi.Core.Managers;
using LoggerApi.Dto.Clients.Requests;
using LoggerApi.Dto.Clients.Responses;
using Microsoft.AspNetCore.Mvc;

namespace LoggerApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientsController : ControllerBase
    {
        private readonly ClientsManager _clientsManager;

        public ClientsController(ClientsManager clientsManager)
        {
            _clientsManager = clientsManager;
        }

        [HttpGet][Route("All")]
        public async Task<IEnumerable<SecureClientInfoResponse>> GetAll()
        {
            return await _clientsManager.GetAllClientsInfo();
        }

        [HttpGet]
        [Route("ByAdminId")]
        public async Task<IEnumerable<FullClientInfoResponse>> GetByAdmin(Guid adminId)
        {
            return await _clientsManager.GetAllClientsInfo(adminId);
        }

        [HttpGet][Route("ById")]
        public async Task<FullClientInfoResponse> GetByIdentityId(Guid id)
        {
            return await _clientsManager.GetClientInfoByIdentityId(id);
        }

        [HttpPost]
        [Route("SetUserInfo")]
        public async Task SetUserInfo([FromBody]NewUserInfoRequest userInfo) 
            => await _clientsManager.SetUserInfo(userInfo);

        [HttpPost]
        [Route("Register")]
        public async Task Register([FromBody] ClientRegistrationRequest newUser) 
            => await _clientsManager.Register(newUser);

        [HttpPut]
        [Route("Login")]
        public async Task<bool> Login([FromBody]ClientLoginRequest credentials) 
            => await _clientsManager.Login(credentials);

        [HttpPut]
        [Route("SignOut")]
        public async Task SignOut([FromBody]Guid id) 
            => await _clientsManager.SignOut(id);
    }
}
