﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LoggerApi.Core.Managers;
using LoggerApi.Core.Models;
using LoggerApi.Core.Models.Db;
using LoggerApi.Dto.Admin.Requests;
using LoggerApi.Dto.Admin.Responses;
using Microsoft.AspNetCore.Mvc;

namespace LoggerApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly AdminsManager _adminsManager;
        private readonly LoggerContext _tempContext;

        public AdminController(AdminsManager adminsManager, LoggerContext tempContext)
        {
            _adminsManager = adminsManager;
            _tempContext = tempContext;
        }

        [HttpPost]
        [Route("GenerateAdmins")]
        public async Task Generate(int count)
        {
            var admins = new List<Admin>();
            for (var i = 0; i < count; i++)
            {
                admins.Add(new Admin
                {
                    EMail = $"test{i}@gmail.com",
                    Login = $"test{i}",
                    Pass = "P@ssword"
                });
            }

            await _tempContext.Admins.AddRangeAsync(admins);
            await _tempContext.SaveChangesAsync();
        }

        [HttpGet]
        [Route("All")]
        public async Task<IEnumerable<AdminMinInfoResponse>> GetAll()
            => await _adminsManager.GetAll();

        [HttpGet][Route("Find")]
        public async Task<AdminInfoResponse> FindAdmin(Guid adminId) 
            => await _adminsManager.FindAdmin(adminId);

        [HttpPost]
        [Route("Register")]
        public async Task<AdminInfoResponse> Register([FromBody] AdminRegistrationRequest newAdmin) 
            => await _adminsManager.Register(newAdmin);

        [HttpDelete]
        public async Task Delete([FromBody]Guid id) 
            => await _adminsManager.Delete(id);
    }
}
