﻿using System.Threading.Tasks;
using Common.Exceptions.ConfigExceptions;
using Common.Validators;
using LoggerApi.Core.Models;
using LoggerApi.Core.Models.Db;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LoggerApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConfigsController : ControllerBase
    {
        private readonly string LogUrl = "LoggerUrl";
        private readonly LoggerContext _context;

        public ConfigsController(LoggerContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("LoggerUrl")]
        public async Task<string> GetLoggerUrl()
        {
            var any = await _context.Configs.AnyAsync(x => x.Name == LogUrl && x.Parameter!=null);
            if (!any) throw new LoggerEmptyUrlResponseEcxeption();

            var config = await _context.Configs.FirstAsync(c=>c.Name == LogUrl);
            return config.Parameter;
        }

        [HttpPost]
        [Route("LoggerUrl")]
        [DisableModelStateCheck]
        public async Task SetLoggerUrl([FromBody] string url)
        {
            var validUrl = url ?? throw new LoggerEmptyUrlRequestEcxeption();
            
            var config = await GetConfigAsync(LogUrl);
            config.Parameter = validUrl;
            await _context.SaveChangesAsync();
        }

        private async Task<Config> GetConfigAsync(string configName)
        {
            var any = await _context.Configs.AnyAsync(x => x.Name == configName);
            Config config;
            if (any)
            {
                config = await _context.Configs.FirstAsync(x => x.Name == LogUrl);
                return config;
            }
            else
            {
                config = new Config { Name = LogUrl };
                config = (await _context.Configs.AddAsync(config)).Entity;
                return config;
            }
        }
    }
}
