﻿using System;
using FluentValidation;
using LoggerApi.Dto.Projects;

namespace LoggerApi.FluentValidators
{
    public class AddProjectRequestValidator : AbstractValidator<AddProjectRequest>
    {
        public AddProjectRequestValidator()
        {
            RuleFor(c => c.AdminId).NotEqual(Guid.Empty);
            RuleFor(c => c.ProjectName).NotEmpty();
        }
    }
}
