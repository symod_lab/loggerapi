﻿using LoggerApi.Core.Managers;
using LoggerApi.Core.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace LoggerApi.DIConfigs
{
    public static class RepositoryRegistrations
    {
        public static void Register(IServiceCollection services)
        {
            services.AddScoped<AdminsRepository>();
            services.AddScoped<ClientsRepository>();

            services.AddScoped<ClientsManager>();
            services.AddScoped<AdminsManager>();
        }
    }
}
