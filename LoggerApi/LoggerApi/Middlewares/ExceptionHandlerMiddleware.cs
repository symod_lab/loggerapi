﻿using System;
using System.Threading.Tasks;
using Common.Exceptions;
using Microsoft.AspNetCore.Http;

namespace LoggerApi.Middlewares
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (LoggerStatusCodeException ex)
            {
                context.Response.StatusCode = (int)ex.StatusCode;
                context.Response.ContentType = @"application/text";
                await context.Response.WriteAsync(ex.Message);
            }
            catch (Exception ex)
            {
                context.Response.StatusCode = 502;
                context.Response.ContentType = @"application/text";
                await context.Response.WriteAsync(ex.Message);
            }
        }
    }
}
