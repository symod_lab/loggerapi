﻿using System.ComponentModel.DataAnnotations;

namespace Common.Validators
{
    public class DisableModelStateCheck : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return true;
        }
    }
}
