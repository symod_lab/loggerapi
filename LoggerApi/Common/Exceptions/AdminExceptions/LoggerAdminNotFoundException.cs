﻿using System.Net;

namespace Common.Exceptions.AdminExceptions
{
    public class LoggerAdminNotFoundException : LoggerStatusCodeException
    {
        public LoggerAdminNotFoundException() : base("Admin is not found", HttpStatusCode.NotFound)
        {
        }
    }
}
