﻿using System.Net;

namespace Common.Exceptions.ConfigExceptions
{
    public class LoggerEmptyUrlRequestEcxeption : LoggerStatusCodeException 
    {
        public LoggerEmptyUrlRequestEcxeption() : base("Url can`t be empty", HttpStatusCode.BadRequest)
        {
        }
    }
}
