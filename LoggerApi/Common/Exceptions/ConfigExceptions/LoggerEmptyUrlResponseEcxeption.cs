﻿using System.Net;

namespace Common.Exceptions.ConfigExceptions
{
    public class LoggerEmptyUrlResponseEcxeption : LoggerStatusCodeException 
    {
        public LoggerEmptyUrlResponseEcxeption() : base("Woops, Url is empty yet", HttpStatusCode.BadGateway)
        {
        }
    }
}
