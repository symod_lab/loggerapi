﻿using System;
using System.Net;

namespace Common.Exceptions
{
    public class LoggerStatusCodeException : Exception
    {
        public LoggerStatusCodeException(string message, HttpStatusCode code) : base(message)
        {
            StatusCode = code;
        }

        public HttpStatusCode StatusCode { get; set; }
    }
}
